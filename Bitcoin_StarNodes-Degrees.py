
# coding: utf-8

# # Bitcoin exploration using GraphX

# [GraphX](https://spark.apache.org/graphx/) is Apache Spark's API for graphs and graph-parallel computation.
# 
# Links:
# - User Guide: http://graphframes.github.io/user-guide.html
# - Examples using Python: http://go.databricks.com/hubfs/notebooks/3-GraphFrames-User-Guide-python.html
#
# To run program
# spark-submit --packages graphframes:graphframes:0.5.0-spark1.6-s_2.11,com.databricks:spark-csv_2.11:1.5.0 Bitcoin_StarNodes-Degrees.py 
#


## Libraries
from collections import namedtuple
import findspark
findspark.init()

import pyspark
from pyspark import SparkContext

from graphframes import *
from pyspark.sql.functions import col, concat, lit, collect_set
from pyspark.sql import SQLContext

def convertToReadableString(r):
    """
    Convert a DataFrame rowrow of graph.inDegrees to CSV
    """
    return r['id'] + ',' + str(r['degree'])

# Parameters
filepath = 'StarNodes_output'

SparkContext.setSystemProperty('spark.memory.fraction', '0.9')
SparkContext.setSystemProperty('spark.executor.memory', '4g')

run_sample = False

vin_file = "/data/bitcoin/vin.csv"
vout_file = "/data/bitcoin/vout.csv"

if run_sample:
    vin_file = "vinsample.csv"
    vout_file = "voutsample.csv"

# Let's initialise spark
sc = pyspark.SparkContext(appName="StarNodes")

# spark = SparkSession.builder.appName("Bitcoin Graph").getOrCreate()
spark = SQLContext.getOrCreate(sc)

vin = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vin_file)

# All vouts will be vertices
vout = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vout_file)

vertices = vout.selectExpr("pubkey as id")

# Transaction with sender information, populating address for sender
# Also, renaming columns to avoid clashes in the future
tsi = vin.join(vout, (vin.tx_hash == vout.hash) & (vin.vout == vout.n)).select(
            col("txid").alias("transaction"), col("tx_hash").alias("previous_tx"),
            col("vout").alias("previous_tx_id"), col("pubkey").alias("previous_tx_address")
        )

edges = tsi.join(vout, (tsi.transaction == vout.hash)).selectExpr("previous_tx_address as src", "pubkey as dst", "CONCAT(previous_tx, '.', previous_tx_id, '|', hash, '.', n ,'|', value) as relationship")

# Lets create the graph
g = GraphFrame(vertices, edges)


output = g.degrees.rdd.sortBy(lambda x: x[1], False)


# Convert it to CSV format and save to HDFS
output.map(lambda r: convertToReadableString(r)).saveAsTextFile(filepath)

#g.inDegrees.rdd.sortBy(lambda a: a[1])
# g.inDegrees.rdd.sortBy(lambda x: x[1], False).sample(False, 0.1).map(lambda r: toCSVLine(r)).saveAsTextFile()
# sortBy(lambda x: x[1], False).
#output = g.inDegrees.rdd.take(10)

#print('****************************** OUTPUT *****************************************')
#print(output.take(10))
