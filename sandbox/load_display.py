import findspark
findspark.init()

import pyspark
from pyspark import SparkContext
SC = pyspark.SparkContext(appName="Bitcoin - display")
output_path = "/user/group-AM/output-triangulation-hashed"

def run():
    # ## Parameters
    # To run program
    # spark-submit --packages graphframes:graphframes:0.5.0-spark1.6-s_2.11,com.databricks:spark-csv_2.11:1.5.0  load_display.py
    
    output = SC.sequenceFile(output_path)
    output.take(10)

if __name__ == '__main__':
    run()
