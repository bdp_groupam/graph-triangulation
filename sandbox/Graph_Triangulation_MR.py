"""
Graph Triangulation
"""

from collections import namedtuple
import findspark
findspark.init()

import pyspark
from pyspark import SparkContext

SparkContext.setSystemProperty('spark.memory.fraction', '0.7')
SparkContext.setSystemProperty('spark.executor.memory', '7g')

SC = pyspark.SparkContext(appName="Bitcoin - Graph Triangulation - Group AM")


# Named tuples below will contain the data from our files
_vin = namedtuple('vin', ['tx', 'previous_tx', 'previous_tx_ix', 'address'])
_vout = namedtuple('vout', ['tx', 'ix', 'value', 'address'])
_coingen = namedtuple('coingen', ['tx', 'sequence'])

# Functions used to process the contents of our files
def coingen_parsing(line):
    elements = line.split(',')
    return _coingen(elements[1], elements[3])

def transaction_parsing(line):
    elements = line.split(',')
    return elements

def vin_parsing(line):
    elements = line.split(',')
    elements.append(None)
    return _vin(*elements)

def vout_parsing(line):
    elements = line.split(',')
    return _vout(elements[0], elements[2], elements[1], elements[3])

def process_csv(file, process_element, alias, remove_header=True):
    # Read the CSV file
    data = SC.textFile(file)

    # Transform each element
    data = data.map(lambda element: process_element(element))

    # Remove the first row (header) of our file
    if remove_header:
        first_row = data.first()
        data = data.filter(lambda element: element[0] != first_row[0])

    return data


# ## Processing Functions

def prepare_transaction_vin(element):
    """
    Populates the wallet address in the transaction input, retrieves the transaction id
    of the element and sets the tuple used by the iteration

    :param: element (tx + ix, (vin.tx, (vout.address, path)))

    :rtype: tuple (tx_id, tx_in, tx_out, path)
    """
    tx_id = element[1][0]
    tx_address = element[1][1][0]
    path = element[1][1][1]

    return (tx_id, (tx_address, path))

def transaction_with_no_aba_pattern(element):
    """
    Filters transactions where we found the following pattern:
    Wallet A -> Wallet B -> Wallet A

    element tuple - (new_id, (vout, path, iteration, found))

    :rtype: bool - True if no ABA Pattern exists, False if not
    """
    path = element[1][1]

    if len(path) <= 2:
        return True

    for i in range(len(path) - 2):
        if path[i] == path[i + 2]:
            return False

    return True


def process_iteration(tx_id, sender_address, receiver, path, iteration=0):
    """
    Final transformation of an iteration. It prepares the input for the next iteration

    :rtype: key value pair - (tx_id, (vout, path, iteration, found))
    """
    new_id = tx_id + receiver[0]
    iteration += 1

    # Adds the sender to the path the first iteration
    if iteration == 1:
        path = [sender_address]

    # Handle case when there are multiple receivers
    # Bug in Spark duplicating addresses
    if len(path) == iteration + 1 and iteration != 1:
        path[iteration] = receiver[1]
    else:
        path.append(receiver[1])

    # Validates if a cycle (triangulation) exists in the path
    found = path.count(path[0]) > 1

    # Our current transaction output will be used as the input in our next iteration
    iteration_output = (receiver[1], path, found)

    return (new_id, iteration_output)

def run():
    # ## Parameters

    running_sample = False

    hadoop_path = "samples/bitcoin/"

    transactions_file = "transactions.csv"
    coingen_file = "coingen.csv"
    vin_file = "vin.csv"
    vout_file = "vout.csv"

    output_path = "/user/group-AM/output-triangulation"

    if not running_sample:
        hadoop_path = "/data/bitcoin/"

    transactions_file = hadoop_path + transactions_file
    coingen_file = hadoop_path + coingen_file
    vin_file = hadoop_path + vin_file
    vout_file = hadoop_path + vout_file

    print(SC._conf.getAll())

    # Load contents of our files
    # transactions = process_csv(transactions_file, transaction_parsing, 'transactions')
    # coingen = process_csv(coingen_file, coingen_parsing, 'coingen')
    vin = process_csv(vin_file, vin_parsing, 'vin')
    vout = process_csv(vout_file, vout_parsing, 'vout')

    # Convert vin and vout to key value pairs (tx|ix, element)
    # Note: The transaction id and index are used to match transactions in vin and vout files
    vin_kv = vin.map(
        lambda element: (element.previous_tx + element.previous_tx_ix, element.tx)
    ).cache()

    vout_kv = vout.map(lambda element: (element.tx + element.ix, (element.address, [])))

    # Used to join a transaction with its outputs
    tx_outputs = vout.map(lambda element: (element.tx, (element.ix, element.address))).cache()


    # ### Iterate

    # Non destructive step.
    # The code below shows you what an iteration does
    # vin_kv.join(vout_kv).map(lambda element: prepare_transaction_vin(element))
    #   .join(tx_outputs).map(lambda e: (e[0], e[1][0][0], e[1][1], e[1][0][2], e[1][0][3]))
    #   .filter(lambda el: transaction_not_to_self(el[0], el[1], el[2]))
    #   .map(lambda e: process_iteration(e[0], e[1], e[2], e[3], e[4])).take(10)

    for i in range(3):

        # Populate transaction inputs with address

        # This step is required in the first iteration to get the sender address
        # In further iterations, gets the next transaction id of a previous vout
        transaction_with_address = vin_kv.join(vout_kv).map(
            lambda element: prepare_transaction_vin(element)
        )

        # Element:
        # (tx_id, (tx_address, path))

        # Populate our transaction inputs with outputs
        transaction_info = transaction_with_address.join(tx_outputs)

        # Element:
        # (tx_id, ((sender.address, path), (receiver.ix, receiver.address)))

        # sender wallet - e[1][0][0],
        # receiver wallet - e[1][1][1],
        # path - e[1][0][1]

        # Remove transactions that send remainder to themselves
        transaction_info = transaction_info.filter(lambda el: el[1][0][0] != el[1][1][1])

        # Prepare the output for the next iteration
        iteration_result = transaction_info.map(
            lambda e: process_iteration(e[0], e[1][0][0], e[1][1], e[1][0][1], i)
        )

        # Only find ABA Pattern after first iteration
        if i >= 1:
            vout_kv = iteration_result.filter(lambda e: transaction_with_no_aba_pattern(e))
        else:
            vout_kv = iteration_result

        # print("*******\n\n\n ", vout_kv.take(10), '\n\n\n*************')


    # ## Output

    # We will first leave only the transaction that have a triangulation in them.
    # Then we transform it to (last transaction, path) and we finish by only
    # leaving unique values.

    # Finishes matching our data
    output = vout_kv.filter(lambda e: e[1][2] == True) \
        .map(lambda el: (el[0], el[1][1])).reduceByKey(lambda a, b: a)

    # Save to folder
    output.saveAsTextFile(output_path)
    # print("********************** OUTPUT ***********************")
    # print(output.take(10))

if __name__ == '__main__':
    run()

