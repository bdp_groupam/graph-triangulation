
# coding: utf-8

# # Bitcoin exploration using GraphX

# [GraphX](https://spark.apache.org/graphx/) is Apache Spark's API for graphs and graph-parallel computation.
# 
# Links:
# - User Guide: http://graphframes.github.io/user-guide.html
# - Examples using Python: http://go.databricks.com/hubfs/notebooks/3-GraphFrames-User-Guide-python.html
#
# To run program
# spark-submit --packages graphframes:graphframes:0.5.0-spark1.6-s_2.11,com.databricks:spark-csv_2.11:1.5.0 Bitcoin_Test.py 
#


## Libraries
from collections import namedtuple
import findspark
findspark.init()

import pyspark
from pyspark import SparkContext

from graphframes import *
from pyspark.sql.functions import col, concat, lit, collect_set
from datetime import datetime
from pyspark.sql import SQLContext


def convertToReadableString(r):
    """
    Convert a DataFrame rowrow of graph.inDegrees to CSV
    """
    return r['id'] + ',' + str(r['degree'])

# Parameters
filepath = 'Test'

SparkContext.setSystemProperty('spark.memory.fraction', '0.9')
SparkContext.setSystemProperty('spark.executor.memory', '4g')

run_sample = True

vin_file = "/data/bitcoin/vin.csv"
vout_file = "/data/bitcoin/vout.csv"

if run_sample:
    vin_file = "vinsample.csv"
    vout_file = "voutsample.csv"
    transactions_file="transactionssample.csv"

# Let's initialise spark
sc = pyspark.SparkContext(appName="Test")

# spark = SparkSession.builder.appName("Bitcoin Graph").getOrCreate()
spark = SQLContext.getOrCreate(sc)

vin = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vin_file)

# All vouts will be vertices
vout = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vout_file)

#transactions times and values would be in our vertices also
transactions = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(transactions_file)

transactions2=transactions.select(col("tx_hash"),(datetime.strptime(str(col("time")),'%b %d %Y %I:%M%p')).alias("time"))

vertices = vout.selectExpr("pubkey as id")

# Transaction with sender information, populating address for sender
# Also, renaming columns to avoid clashes in the future
tsi = vin.join(vout, (vin.tx_hash == vout.hash) & (vin.vout == vout.n)).select(
            col("txid").alias("transaction"), col("tx_hash").alias("previous_tx"),
            col("vout").alias("previous_tx_id"), col("pubkey").alias("previous_tx_address"),col("hash").alias("previous_transaction_number")
                                                                                           )

ta=tsi.join(transactions2, (transactions2.tx_hash==tsi.transaction))

edges = ta.join(vout, (ta.transaction == vout.hash)).selectExpr("CONCAT(previous_tx_address,'|',hash) as src", "CONCAT(pubkey,'|',value) as dst", "time as relationship")

# Lets create the graph
g = GraphFrame(vertices, edges)


output = g.edges.rdd


# Convert it to CSV format and save to HDFS
output.map(lambda r: r).saveAsTextFile(filepath)
