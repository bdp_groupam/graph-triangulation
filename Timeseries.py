
# coding: utf-8


from collections import namedtuple
import findspark
findspark.init()

import pyspark
from pyspark import SparkContext

SC = pyspark.SparkContext(appName="Bitcoin - Triangulation - Group AM - TS")
SparkContext.setSystemProperty('spark.executor.memory', '4g')
SparkContext.setSystemProperty('spark.executor.cores', '2')
SparkContext.setSystemProperty('spark.task.maxFailures', '20')

# Named tuples below will contain the data from our files
_vin = namedtuple('vin', ['tx', 'previous_tx', 'previous_tx_ix', 'address'])
_vout = namedtuple('vout', ['tx', 'ix', 'value', 'address'])
_coingen = namedtuple('coingen', ['tx', 'sequence'])
_trx = namedtuple('trx', ['hash', 'time', 'date' ,'ins', 'outs'])
time_from = 1383264000 #1/11/2013
time_to = 1391212799 #31/1/2014
time_format = '%Y-%m-%d'
running_sample = False
hadoop_path = "./"
transactions_file = "transactions.csv"
output_path = "/user/group-AM/output-timeseries-pytest"
if not running_sample:
    hadoop_path = "/data/bitcoin/"
transactions_file = hadoop_path + transactions_file


import datetime
def process_csv(file, process_element, alias, remove_header=True):
    # Read the CSV file
    data = SC.textFile(file)

    # Transform each element
    data = data.map(lambda element: process_element(element))

    # Remove the first row (header) of our file
    if remove_header:
        first_row = data.first()
        data = data.filter(lambda element: element[0] != first_row[0])
    return data

def trx_parsing(line): 
    elements = line.split(',')
    #('hash', 'time', 'date', 'ins', 'outs')
    try: 
        date = datetime.datetime.fromtimestamp( float(elements[2]) ).strftime(time_format)
    except ValueError:
        date = ''
    return _trx(hash(elements[0])*1000, elements[2], date, elements[3], elements[4])


def run():
    # To Run this job:
    # spark-submit Timeseries.py

    trx = process_csv(transactions_file, trx_parsing, 'trx')
    trx = trx.filter(lambda el: long(el[1]) >=time_from and long(el[1])<=time_to).cache()
    #('hash', 'time', 'date', 'ins', 'outs')
    #Reduce by day
    timeseries = trx.map(lambda el: (el[2], 1)).reduceByKey(lambda a,b: a+b).sortBy(lambda x: x[0], True)
    timeseries.repartition(1).saveAsTextFile(output_path)

if __name__ == '__main__':
    run()
