
# coding: utf-8

# # Bitcoin exploration using GraphX

# [GraphX](https://spark.apache.org/graphx/) is Apache Spark's API for graphs and graph-parallel computation.
# 
# Links:
# - User Guide: http://graphframes.github.io/user-guide.html
# - Examples using Python: http://go.databricks.com/hubfs/notebooks/3-GraphFrames-User-Guide-python.html
#
# To run program
# spark-submit --packages graphframes:graphframes:0.5.0-spark1.6-s_2.10,com.databricks:spark-csv_2.10:1.5.0 Triangle_Return.py 
#


## Libraries
from collections import namedtuple
import findspark
findspark.init()

import pyspark
from pyspark import SparkContext

from graphframes import *
from pyspark.sql.functions import col, concat, lit, collect_set
from pyspark.sql import SQLContext

# Parameters
filepath= 'Triangle_Return'

SparkContext.setSystemProperty('spark.memory.fraction', '0.9')
SparkContext.setSystemProperty('spark.executor.memory', '4g')

run_sample = True

vin_file = "/data/bitcoin/vin.csv"
vout_file = "/data/bitcoin/vout.csv"
transactions_file="/data/bitcoin/transactions.csv"

if run_sample:
    vin_file = "vinsample.csv"
    vout_file = "voutsample.csv"
    transactions_file="transactionssample.csv"

# Let's initialise spark
sc = pyspark.SparkContext(appName="Bitcoin-GraphFrames")

# spark = SparkSession.builder.appName("Bitcoin Graph").getOrCreate()
spark = SQLContext.getOrCreate(sc)

vin = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vin_file)

# All vouts will be vertices
vout = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vout_file)
transactions=spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(transactions_file)
vertices = vout.selectExpr("pubkey as id")

# Transaction with sender information, populating address for sender
# Also, renaming columns to avoid clashes in the future
tsi = vin.join(vout, (vin.tx_hash == vout.hash) & (vin.vout == vout.n)).select(
            col("txid").alias("transaction"), col("tx_hash").alias("previous_tx"),
            col("vout").alias("previous_tx_id"), col("pubkey").alias("previous_tx_address")
        )
ta=tsi.join(transactions, (transactions.tx_hash == tsi.transaction))
#"CONCAT(previous_tx, '.', previous_tx_id, '|', hash, '.', n ,'|',time,'|', value ) as relationship \n"
edges = ta.join(vout, (ta.transaction == vout.hash)).selectExpr("previous_tx_address as src", "pubkey as dst", "CONCAT(hash,'|', value ) as relationship \n")



# Lets create the graph
g = GraphFrame(vertices, edges)


paths=g.find("(a)-[e]->(b);(b)-[e2]->(c);(c)-[e3]->(a)")
e1=paths.drop_duplicates()
##e2 = e1.select("e.src", "e.dst", "e.relationship")
e2=e1.select("e.*","e2.*","e3.*")
g2=GraphFrame(vertices,e2)
g2.edges.rdd.saveAsTextFile(filepath)


#filteredPaths = g.bfs(
 # fromExpr = "",
  #toExpr = "id='{1AEoiHY23fbBn8QiJ5y6oAjrhRY1Fb85uc}'",
  #edgeFilter = "",
  #maxPathLength = 1)

#filteredPaths.edges.rdd.map(lambda r:r).repartition(1).saveAsTextFile(filepath2)


#res.reducebykey(lambda a, b:a).saveAsTextFile(filepath2)
# Convert it to CSV format and save to HDFS
#output.map(lambda r: convertToReadableString(r)).saveAsTextFile(filepath)



