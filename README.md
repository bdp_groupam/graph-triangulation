# BDP 02 - Analysis of Bitcoin Transactions - Group: AM

The initial goal of this project is to extract the transactions from the raw Bitcoin format and model them as a directed graph. Once this graph has been built, we will explore circular flows of transactions by identifying the triangles in the graph.

# Python Scripts

To run the Python script created to do the Triangulation using Map Reduce:
```bash
# Navigate to repository folder
cd /path/to/repo/graph-triangulation
spark-submit Graph_Triangulation_MR.py
```
To run the python scripts created to do additional analysis with GraphFrames:
```bash
spark-submit --packages graphframes:graphframes:0.5.0-spark1.6-s_2.10,com.databricks:spark-csv_2.10:1.5.0 [file_name].py
```
# Submitted scripts 
## 1.Graph_Triangulation_MR.py

This is the triangulation aglorithm. It takes around 2 hours to process the entire dataset and outputs a compressed sequence file containing around 4 million triangulations 
The code does not check if the output directory name exists. It uses this path: "/user/group-AM/output-triangulation-hashed-trimmed" 

## 2.Bitcoin_StarNodes-Degrees.py
This script uses Graphframes and GraphX to calculate the total indegrees across the entire dataset. The output is sampled on /results/top100_indegrees.xlxs

## 3.Ransomware-test.py
This is a GraphX traversal algorithm that traces a ransomeware address upwards and downwards to find its victims and the transaction hierarchy. 

## 4.Timeseries.py
This script outputs the number of daily transactions across a date range. We run in on different periods and across the entire dataset. 

## 5.Triangle_Return.py
An attemp to find triangulations using GraphX and Graphframes. Does not complete

# Jupyter Notebooks

Within the [notebooks](./notebooks/) folder, we include various notebooks [Bitcoin - Graph Triangulation](./Bitcoin\ -\ Graph\ Triangulation.ipynb), [exploration - transactions](./exploration\ -\ transactions.ipynb), that were used for our initial data exploration and to get familiar with the structure of our dataset.

We also used Jupyter Notebooks to further analyse the output created by our [Triangulation using Map Reduce](./Graph_Triangulation.py) python script.

# Output 
## /user/group-AM/output-triangulation-hashed-boettget
The first successful run of triangulations across the entire dataset. Special thanks to Timm Boettger
The output is a compressed sequence file. 

# Output form:
 (hash(trx_id), abca)
-196900324112900070     
{192zJzm3mo8J4qDVrNArFV7Yb7G7DU5SKp},{1dice97ECuByXAvqXpaYzSaQuPVvrtmz6},{1zxN4RheeAy5B7wdBSVa5ToQx4pVvA3oN},{192zJzm3mo8J4qDVrNArFV7Yb7G7DU5SKp}

# To view a sample: 
```bash
 hadoop fs -text /user/group-AM/output-triangulation-hashed-boettget/part-00000  | head -n 20
```

## /user/group-AM/output-triangulation-hashed-trimmed
Triangulation after trimming the last address of the sequence abca

## /user/group-AM/output-timeseries-full
The daily transaction volume timeseries on the entire dataset. 

## /user/group-AM/StarNodes_output 
Starnodes on the entire dataset. 

## /user/group-AM/Ransomeware_inTrans, /user/group-AM/Ransomeware_outTrans
A trace of Ramsomeware in and out transactions of the address [1AEoiHY23fbBn8QiJ5y6oAjrhRY1Fb85uc](https://blockchain.info/address/1AEoiHY23fbBn8QiJ5y6oAjrhRY1Fb85uc) 

## /user/group-AM/output_indegrees_2 
The total indegrees of most of the dataset. 

# Bitcoin

Bitcoin (BTC) is a worldwide cryptocurrency and digital payment system called the first decentralized digital currency, as the system works without a central repository or single administrator. 

BTC value have doubled in the last 60 days and increased about 8 folds in the last 12 months. Gaining an unprecedented amount of attention from investors and regulators. It is the cryptocurrency that has the highest market cap and the most network effect. 

### Primary analysis

The primary objective of the analysis is to identify triangular flows in bitcoin transactions (i.e. from ‘A’ ,‘B’, ‘C’ back to ‘A’). Once these triangular transactions have been identified, an exploratory analysis will be undertaken to evaluate interesting patterns within the transactions. Examples might involve volumes of bitcoins traded, graph centrality, identifying wallets involved with the most triangular transactions (additional analyses proposed below. A full evaluation of the dataset will determine how much of it can be feasibly implemented).


## SCHEMA

**Blocks**

*height (the block number), hash (unique ID for the block), time (the time at which the block was created), difficulty (the complexity of the math problem associated with the block)*

0,  000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f,  1231006505,  1


**Coingen**

*db_id(from the database, ignore), tx_hash (unique id for the coin gen), coinbase (source of the coin), sequence (ignore)*

1,  0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098,  04ffff001d0104,  4294967295

For the initial part of the coursework only the wallets involved in the transactions are of interest. However, many of the other fields may provide some interesting insight during the secondary exploration.


**Transactions**

*tx_hash (unique id for the transaction), blockhash (block this transaction belongs to), time (time of the transaction), tx_in_count (number of transactions coming in), tx_out_count (number of outgoing wallets)*

0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098,00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048,1231469665,1,1



**VIN**

*txid (the associated transaction), tx_hash (The transaction the coins come from), vout (id in list of outputs)*

f4184fc596403b9d638783cf57adfe4c75c605f6356fbc91338530e9831e9e16,0437cd7f8525ceed2324359c2d0ba26006d92d856a9c20fa0241106ee5a597c9,0



**VOUT**

*hash (the associated transaction), value (the amount of bitcoins sent), n (id in list of outputs - same as vout above), publicKey (the wallet id)*

0e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098, 50, 0, {12c6DSiU4Rq3P4ZxziKxzrL5LmMBrzjrJX}


