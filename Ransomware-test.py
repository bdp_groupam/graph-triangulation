
# coding: utf-8

# # Bitcoin exploration using GraphX

# [GraphX](https://spark.apache.org/graphx/) is Apache Spark's API for graphs and graph-parallel computation.
# 
# Links:
# - User Guide: http://graphframes.github.io/user-guide.html
# - Examples using Python: http://go.databricks.com/hubfs/notebooks/3-GraphFrames-User-Guide-python.html
#
# To run program
# spark-submit --packages graphframes:graphframes:0.5.0-spark1.6-s_2.11,com.databricks:spark-csv_2.11:1.5.0 Ransomware-test.py 
#


## Libraries
from collections import namedtuple
import findspark
findspark.init()

import pyspark
from pyspark import SparkContext

from graphframes import *
from pyspark.sql.functions import col, concat, lit, collect_set
from pyspark.sql import SQLContext

def convertToReadableString(r):
    """
    Convert a DataFrame rowrow of graph.Degrees to CSV
    """
    return r['id'] + ',' + str(r['degree'])

# Parameters
filepath2= '/user/group-AM/Ransomeware_outTrans'

SparkContext.setSystemProperty('spark.memory.fraction', '0.9')
SparkContext.setSystemProperty('spark.executor.memory', '4g')

run_sample = False

vin_file = "/data/bitcoin/vin.csv"
vout_file = "/data/bitcoin/vout.csv"
transac_file= "/data/bitcoin/transactions.csv"

if run_sample:
    
   vin_file = "vin_s.csv"
   vout_file = "vout_s.csv"
   transac_file = "tran_s.csv"

# Let's initialise spark
sc = pyspark.SparkContext(appName="Bitcoin-GraphFrames")

# spark = SparkSession.builder.appName("Bitcoin Graph").getOrCreate()
spark = SQLContext.getOrCreate(sc)

vin = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vin_file)

# All vouts will be vertices
vout = spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(vout_file)
transactions=spark.read.format("com.databricks.spark.csv").option("header", "true").option("inferSchema", "true").load(transac_file)
vertices = vout.selectExpr("pubkey as id")

# Transaction with sender information, populating address for sender
# Also, renaming columns to avoid clashes in the future
tsi = vin.join(vout, (vin.tx_hash == vout.hash) & (vin.vout == vout.n)).select(
            col("txid").alias("transaction"), col("tx_hash").alias("previous_tx"),
            col("vout").alias("previous_tx_id"), col("pubkey").alias("previous_tx_address")
        )
ta=tsi.join(transactions, (transactions.tx_hash == tsi.transaction))

edges = ta.join(vout, (ta.transaction == vout.hash)).selectExpr("previous_tx_address as src", "pubkey as dst", "CONCAT(hash,'|', value ) as relationship \n")



# Lets create the graph
g = GraphFrame(vertices, edges)

#Create a subgraph from the wallet id 
#Ransomeware wallet: Cryptolocker    1AEoiHY23fbBn8QiJ5y6oAjrhRY1Fb85uc    October 2013 – September 2014


paths=g.find("(a)-[e]->(b)").filter("a.id='{1AEoiHY23fbBn8QiJ5y6oAjrhRY1Fb85uc}'")
e1=paths.drop_duplicates()
##e2 = e1.select("e.src", "e.dst", "e.relationship")
e2=e1.select("e.*")
g2=GraphFrame(vertices,e2)
g2.edges.rdd.map(lambda r:r).repartition(1).saveAsTextFile(filepath2)


# Convert it to CSV format and save to HDFS
#output.map(lambda r: convertToReadableString(r)).saveAsTextFile(filepath)



